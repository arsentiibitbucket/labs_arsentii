'use strict';

module.exports = function (object, lineItem) {
    Object.defineProperty(object, 'custom', {
        enumerable: true,
        value: lineItem.custom
    });
};
