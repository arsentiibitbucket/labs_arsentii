"use strict";
const server = require("server");
const URLUtils = require("dw/web/URLUtils");

server.get("ShowFolder", function (req, res, next) {
    const ContentMgr = require('dw/content/ContentMgr'); 
    const folderID = request.httpParameterMap.fdid.stringValue;
    const folder = ContentMgr.getFolder(folderID)

    if(empty(folder)) {
        res.render('Lab6.3/error')
    } else {
        res.render('Lab6.3/foldercontent',{
            folder:folder
        })
    }
  
    next();
});

module.exports = server.exports();
