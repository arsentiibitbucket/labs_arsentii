"use strict";

var server = require('server');

server.get("Show", function (req, res, next) {
    const URLUtils = require("dw/web/URLUtils");
    let actionUrl = URLUtils.url("EmailSubscribe-Subscribe"); 
    let newsletterForm = server.forms.getForm("emailSignup"); 
    newsletterForm.clear();

    res.render("Newsletter/newsletter", {
        actionUrl: actionUrl,
        newsletterForm: newsletterForm,
    });
    next();
});

/**
 * @namespace EmailSubscribe
 */


/**
 * Checks if the email value entered is correct format
 * @param {string} email - email string to check if valid
 * @returns {boolean} Whether email is valid
 */
function validateEmail(email) {
    var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    return regex.test(email);
}

/**
 * EmailSubscribe-Subscribe : The EmailSubscribe-Subscribe enpoint allows the shopper to submit their eamil address to be added to a mailing list. OOB SFRA does not have a mailing list feature however this endpoint call a hook would allow for a customer to easily allow for custiomization
 * @name Base/EmailSubscribe-Subscribe
 * @function
 * @memberof EmailSubscribe
 * @param {httpparameter} - emailId - Input field, The shopper's email address
 * @param {category} - sensitive
 * @param {returns} - json
 * @param {serverfunction} - post
 */
server.post("Subscribe", function (req, res, next) {
    var Resource = require("dw/web/Resource");
    var hooksHelper = require("*/cartridge/scripts/helpers/hooks");

    const form = server.forms.getForm('emailSignup');
    let email = form.email.htmlValue;
    let firstname = form.firstname.htmlValue;
    let gender = form.gender.htmlValue;
    var isValidEmailid;
    let resp;
    if (email) {
        isValidEmailid = validateEmail(email);

        if (isValidEmailid) {
            resp = hooksHelper("app.mailingList.subscribe", "subscribe", [email,firstname,gender], function () {}); 
            if (resp.code === Resource.msg("code.exist", "status", null)) {
                res.json({
                    error: true,
                    msg: Resource.msg("subscribe.email.exist","homePage",null),
                });
            } else if (resp.code === Resource.msg("code.customObj", "status", null)) {
                res.json({
                    error: true,
                    msg: Resource.msg("subscribe.email.customObj.error","homePage",null),
                });
            } else {
                res.json({
                    success: true,
                    msg: Resource.msg("subscribe.email.success", "homePage", null),
                });
            }
        } else {
            res.json({
                error: true,
                msg: Resource.msg("subscribe.email.invalid", "homePage", null),
            });
        }
    } else {
        res.json({
            error: true,
            msg: Resource.msg("subscribe.email.invalid", "homePage", null),
        });
    }

    next();
});

module.exports = server.exports();
