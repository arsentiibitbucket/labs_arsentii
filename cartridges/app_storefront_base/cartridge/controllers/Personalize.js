"use strict";
const server = require("server");
const URLUtils = require("dw/web/URLUtils");

server.get("Start", function (req, res, next) {
    //let actionUrl = URLUtils.url("NewsLetter-Show"); //sets the route to call for the form submit action
    let personalizeForm = server.forms.getForm('personalize'); //creates empty JSON object using the form definition
   //personalizeForm.clear();

    res.render("Lab9/personalizationdialog", {
        ContinueURL: URLUtils.url('Personalize-Form'),//continueURL
        personalizeForm: personalizeForm,
    });
    next();
});

server.get("Start2", function (req, res, next) {
    //let actionUrl = URLUtils.url("NewsLetter-Show"); //sets the route to call for the form submit action
    let personalizeForm = server.forms.getForm('personalize2'); //creates empty JSON object using the form definition
   //personalizeForm.clear();

    res.render("Lab9/personalizationdialog2", {
        ContinueURL: URLUtils.url('Personalize-Form2'),//continueURL
        personalizeForm: personalizeForm,
    });
    next();
});

server.post("Form", function (req, res, next){
    const form = server.forms.getForm('personalize')

    try{
        const [name, color, font] = [form.pName.htmlValue,form.pColor.htmlValue, form.pFont.htmlValue ]
    } catch(e) {
        return
    }
    res.json({
        success:true,
        pName: name,
        pColor: color,
        pFont: font
    })


    next()
})

server.post("Form2", server.middleware.https, function (req, res, next){
    const form = server.forms.getForm('personalize2')

    try{
        const [name, color, font] = [form.pName.htmlValue,form.pColor.htmlValue, form.pFont.htmlValue ]
    } catch(e) {
        return
    }
    res.json({
        success:true,
        pName: name,
        pColor: color,
        pFont: font
    })


    next()
})

module.exports = server.exports();
