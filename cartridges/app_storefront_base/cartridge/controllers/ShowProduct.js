"use strict";

/**
 * @namespace ShowProduct
 */
 const server = require('server');
 const pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

/**
 * ShowProduct-Start : This endpoint is called when a shopper navigates to the home page
 * @name base/ShowProduct-Start
 * @function
 * @memberof ShowProduct
 * @param {renders} - isml
 * @param {querystringparameter} - pid - Product ID
 * @param {serverfunction} - get
 */


server.get('Start', function(req,res,next){
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var product = ProductFactory.get({ pid: req.querystring.pid });
    if(!product){
        res.setStatusCode(404)
        res.render('WT2.4/productnotfound', {
            Log: 'The product was not found, id:' + req.querystring.pid
        })
    } else {
    res.render('WT2.4/product', {
            product: product
        })
    }
    next();
}, pageMetaData.computedPageMetaData)


module.exports = server.exports()