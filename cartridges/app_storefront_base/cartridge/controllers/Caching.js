'use strict'

/**
 * @namespace Caching
 */
 const server = require('server');
 const pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
/**
 * Applies the default expiration value for the page cache.
 * @name base/Caching-Start
 * @function
 * @memberof Caching
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
*/

server.get('Start', function(req, res, next){
    res.render('WT3.3/cachedpage.isml')
    next()
}, pageMetaData.computedPageMetaData)

server.get('RemoteInclude', function(req, res, next){
    const BasketMgr = require('dw/order/BasketMgr');
    const CartModel = require('*/cartridge/models/cart');
    const Resource = require('dw/web/Resource');

    var currentBasket = BasketMgr.getCurrentBasket();
    var basketModel = new CartModel(currentBasket);
    
    if(basketModel.numItems === 0) {
        res.setStatusCode(404)
        res.json({ errorMessage: Resource.msg('error.cart.empty', 'cart', null) });
    } else {
        res.render('WT3.4/remoteinclude', {
            basket: basketModel
        })
    } 
    next()
})

module.exports = server.exports()