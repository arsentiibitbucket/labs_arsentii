/**
 * A simple form controller.
 *
 */

"use strict";
const server = require("server");
const URLUtils = require("dw/web/URLUtils");

server.get("Start", function (req, res, next) {
    let actionUrl = URLUtils.url("NewsLetter-Show"); //sets the route to call for the form submit action
    let walkthrough5 = server.forms.getForm("walkthrough5"); //creates empty JSON object using the form definition
    walkthrough5.clear();

    res.render("WT5/contactus", {
        actionUrl: actionUrl,
        contactForm: walkthrough5,
    });
    next();
});

server.post("Show", function (req, res, next) {
    const UUIDUtils = require('dw/util/UUIDUtils'); // require use '
    const CustomObjectMgr = require('dw/object/CustomObjectMgr');
    const Logger = require('dw/system/Logger');
    const Transaction = require('dw/system/Transaction');
    const form = server.forms.getForm('walkthrough5')

    const [firstname, lastname, email] = [form.firstname.htmlValue,form.lastname.htmlValue, form.email.htmlValue ]

    let customObject = {};
    let newsLetterlogger = Logger.getLogger('NewsLetter'); // NewsLetterLogger var

    newsLetterlogger.debug(
        'Input params firstname: {0} lastName: {1} email: {2}',
        firstname,
        lastname,
        email
    );

    Transaction.wrap(function () {
        try {
            customObject = CustomObjectMgr.createCustomObject(
                'NewsletterSubscription',
                UUIDUtils.createUUID()
            );
            customObject.custom.firstName = firstname;
            customObject.custom.lastName = lastname;
            customObject.custom.email = email;
        } catch (err) {
            newsLetterlogger.error('NewsLetter subscription faild: {}', err.message);
        }
    });

    res.render('WT5/contactResult', {
        firstname: firstname,
        lastname: lastname,
        email: email,
    });

    next();
});

server.post("subscribe", function (req, res, next) {
    

    form = req.form
    next();
});

module.exports = server.exports();
