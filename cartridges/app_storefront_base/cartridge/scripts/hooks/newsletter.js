"use strict";

/** @module subscribe */

exports.subscribe = function (email,firstname,gender) {
    const UUIDUtils = require("dw/util/UUIDUtils");
    const Logger = require("dw/system/Logger");
    const Transaction = require("dw/system/Transaction");
    const Status = require("dw/system/Status");
    const StatusItem = require("dw/system/StatusItem");
    const CustomObjectMgr = require("dw/object/CustomObjectMgr");
    const Resource = require("dw/web/Resource");

    let specificObj = CustomObjectMgr.getCustomObject("EmailSubscribe", email);

    if (!!specificObj) {
        return new Status(Status.ERROR, Resource.msg("code.exist", "status", null));
    }

    Transaction.wrap(function () {
        try {
            let newsletterObject = CustomObjectMgr.createCustomObject(
                "EmailSubscribe",
                email
            );
            
            newsletterObject.custom.firstName = firstname;
            newsletterObject.custom.gender = gender;
            return {
                status: new Status(Status.OK),
                result: newsletterObject,
            };
        } catch (err) {
            return new Status(Status.ERROR, Resource.msg("code.customObj", "status", null));
        }
    });

    return new Status(Status.OK);
};
